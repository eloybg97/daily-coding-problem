import math
from numpy.testing import assert_array_almost_equal

#Bonus: The angle will be zero twenty three times each day (when hour and minutes are equals)
def get_angle(time):
    time_splitted = time.split(':')

    hour = int(time_splitted[0])
    minute = int(time_splitted[1])
   
    hour_rad = (hour * 2 * math.pi) / 12
    minute_rad = (minute * 2 * math.pi) / 60

    abs_rad = abs(hour_rad - minute_rad)

    return abs_rad % math.pi if abs_rad > math.pi else abs_rad

def main():
    assert_array_almost_equal(get_angle("18:45"), math.pi * 0.5)
    assert_array_almost_equal(get_angle("00:00"), 0)
    assert_array_almost_equal(get_angle("21:00"), math.pi * 0.5)
    assert_array_almost_equal(get_angle("21:30"), math.pi * 0.5)
    assert_array_almost_equal(get_angle("15:45"), math.pi)
    assert_array_almost_equal(get_angle("18:15"), math.pi * 0.5)

    print("It's correct!")




if(__name__ == "__main__"):
    main()

