#include<stdlib.h>
#include <stdio.h>

struct node {
	char* value;
	struct node* right;
	struct node* left;
};

struct node* newNode(char* value) {
	struct node* n = (struct node*) malloc(sizeof(struct node));
	n->value = value;
	n->left = NULL;
	n->right = NULL;

	return n;
}

void find(struct node* root, int level, int* maxLevel, struct node** res) {
	if(root != NULL) {

		if(level > *maxLevel) {
			
			*maxLevel = level;
			*res = root;
		}

		find(root->left, level + 1, maxLevel, res);
		find(root->right, level + 1, maxLevel, res);
	}
}


int main() {
	struct node* root;
	struct node* deepest;

	int maxLevel = -1;

	root = newNode("a");
	root->right = newNode("c");
	root->left = newNode("b");
	root->left->left = newNode("d");

	find(root, 0, &maxLevel, &deepest);
	printf("The deepest node is: %s\n", deepest->value);
}
