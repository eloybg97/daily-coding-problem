#include <iostream>
#include <list>
#include <cmath>

using namespace std;

ostream& operator<<(ostream& ostr, const list<int>& list);
void reverse(list<int> &lst, const int &i, const int &j);
void sort(list<int> &lst);

int main() {
	list<int> data = {2, 1, 3, 2, 5, 6, 7, 1};

	cout << data << endl;
	sort(data);
	cout << data << endl;
}

ostream& operator<<(ostream& ostr, const list<int>& list)
{
    for (const int i : list)
        ostr << ' ' << i;

    return ostr;
}

/**
 * @pre 	i < j
 */
void reverse(list<int> &lst, const int &i, const int &j) {
	int length = abs(i - j);
	int aux;

	int a = i;
	int b = j;

	list<int>::iterator it_i = lst.begin();
	list<int>::iterator it_j = lst.begin();

	for(int k = 0; k < i; ++k){
		++it_i;
		++it_j;
	}

	for(int k = 0; k < j - i; ++k){
		++it_j;
	}

	while(it_i != it_j && a < b) {
		aux = *it_j;
		*it_j = *it_i;
		*it_i = aux;

		++it_i;
		++a;
		--b;
		--it_j;
	}
}


void sort(list<int> &lst) {
	bool sorted;
	int value;
	int a = 0;
	int b = 0;

	list<int>::iterator it;

	a = 0;

	do {
		sorted = true;
		it = lst.begin();
		while(it != lst.end()) {
			value = *it;
			++it;
			++b;

			if(value > *it) {
				sorted = false;
				reverse(lst, a, b);
				a = b;
			}

			else {
				++a;
			}
		}
	} while(!sorted);
}
