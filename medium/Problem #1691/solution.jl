function next_biggest(n)
	next = 0

	mask = 2 ^ (count_ones(n) - 1) + 1
	aux = mask << Int(ceil(log(n)/log(2)) - count_ones(n))

	if(count_ones(n) == 1)
		aux = aux - 1
	end

	# Adjacent bits?
	if(aux == n)
		next = (mask+1) << Int(ceil(log(n)/log(2)) - count_ones(n)) + 1

	else
		offset = trailing_zeros(n)
		i = 1

		while (1 << (offset + 1) & n) == 1
			i = i + 1
		end

		next = ((1 << (offset + i)) |  n) & ~ ((1 << offset + i - 1) & n)

	end

	return next
end


@assert next_biggest(5) == 6
@assert next_biggest(4) == 8
@assert next_biggest(6) == 9
@assert next_biggest(10) == 12

print("It's correct!")
