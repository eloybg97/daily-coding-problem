#include<iostream>
#include<list>

using namespace std;

 
ostream& operator<<(ostream& ostr, const list<int>& list)
{
    for (const int i : list)
        ostr << ' ' << i;

    return ostr;
}

int main() {
	list<int> values = {1 ,5, 4, 3, 2, 6};
	list<int>::const_iterator it;
	list<int> res;

	cout << "Initially: " << values << endl;

	values.sort();

	it = values.begin();
	++it;
	int i = 1;
	int v = 0;

	while(it != values.end()) {
		if(i % 2 == 0) {
			v = *it;
			it = values.erase(it);
			it = values.insert(--it, v);
			++it;
		}

		++it;
		++i;
	}

	cout << "Solution: " << values << endl;;

	

	return 0;	
}
