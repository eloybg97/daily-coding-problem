def cons(a, b):
    def pair(f):
        return f(a, b)
    return pair


def car(pair):
    f = lambda a, b: a
    return pair(f)

def cdr(pair):
    f = lambda a, b: b
    return pair(f)

def main():
    assert car(cons(3, 4)) == 3, "car definition is wrong"
    assert cdr(cons(3, 4)) == 4, "cdr definition is wrong"

    print("Correct!")


if(__name__ == '__main__'):
    main()
